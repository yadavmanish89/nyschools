//
//  SchoolListViewModel.swift
//  NYSchools
//
//  Created by manish yadav on 3/16/23.
//

import Foundation

class SchoolListViewModel {
    private var dataModel = [SchoolModel]()
    let schoolListNetworkManager: SchoolListNetworkManager
    init(schoolListNetworkManager: SchoolListNetworkManager) {
        self.schoolListNetworkManager = schoolListNetworkManager
    }
    var updateUI: (() -> ())?
    var showError: ((String) -> ())?
    func fetchSchoolList(request: RequestProtocol) {
        Task {
            do {
                let result = try await self.schoolListNetworkManager.fetchSchoolList(request: request)
                switch result {
                case .success(let dataModel):
                    self.dataModel = dataModel
                    self.updateUI?()
                case .failure(let error):
                    debugPrint("Error:\(error)")
                    self.showError?(error.description)
                }
            } catch(let error) {
                debugPrint("Error:\(error)")
                self.showError?(error.localizedDescription)
            }
        }
    }
     
    func itemAtIndexPath(index: Int) -> SchoolModel {
        return self.dataModel[index]
    }
    func numberOfRows() -> Int {
        return self.dataModel.count
    }
    func getSchoolListRequest() -> RequestProtocol {
        return APIRequest.schoolList
    }
}
