//
//  SchoolListNetworkManager.swift
//  NYSchools
//
//  Created by manish yadav on 3/16/23.
//

import Foundation

protocol SchoolListNetworkProtocol {
    func fetchSchoolList(request: RequestProtocol) async throws -> Result<[SchoolModel], NetworkError>
}
class SchoolListNetworkManager: SchoolListNetworkProtocol {
    private let networkManager: NetworkProtocol
    
    init(networkManager: NetworkProtocol) {
        self.networkManager = networkManager
    }
    
    func fetchSchoolList(request: RequestProtocol) async throws -> Result<[SchoolModel], NetworkError> {
        guard let data = try await networkManager.request(request: request) else {
            return .failure(.invalidResponse)
        }
        guard let dataModel = JsonParser.getModelFor(data: data, model: [SchoolModel].self) else {
            return .failure(.parsingError)
        }
        return .success(dataModel)
    }
}
