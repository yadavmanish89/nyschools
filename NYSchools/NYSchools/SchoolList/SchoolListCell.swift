//
//  SchoolListCell.swift
//  NYSchools
//
//  Created by manish yadav on 3/16/23.
//

import UIKit

class SchoolListCell: UITableViewCell {
    @IBOutlet weak var schoolName: UILabel!
    @IBOutlet weak var schoolAddress: UILabel!

    var dataModel: SchoolModel? {
        didSet {
            self.setData()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    private func setData() {
        self.schoolName.text = self.dataModel?.schoolName
        let primaryAddress = self.dataModel?.primaryAddressLine1 ?? ""
        let city = self.dataModel?.city ?? ""
        let zip = self.dataModel?.zip ?? ""
        let address = "\(primaryAddress), \(city) \(zip)"
        self.schoolAddress.text = address
    }
}
