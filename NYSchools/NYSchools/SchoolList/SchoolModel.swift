//
//  SchoolModel.swift
//  NYSchools
//
//  Created by manish yadav on 3/16/23.
//

import Foundation

struct SchoolModel: Codable {
    var dbn: String?
    var schoolName: String?
    var phoneNumber: String?
    var primaryAddressLine1: String?
    var city: String?
    var zip: String?
}
