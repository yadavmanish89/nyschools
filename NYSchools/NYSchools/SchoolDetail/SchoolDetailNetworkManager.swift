//
//  SchoolDetailNetworkManager.swift
//  NYSchools
//
//  Created by manish yadav on 3/16/23.
//

import Foundation


protocol SchoolDetailNetworkProtocol {
    func fetchSchoolDetail(request: RequestProtocol) async throws -> Result<[SchoolDetailModel], NetworkError>
}
class SchoolDetailNetworkManager: SchoolDetailNetworkProtocol {
    private let networkManager: NetworkProtocol
    
    init(networkManager: NetworkProtocol) {
        self.networkManager = networkManager
    }
    func fetchSchoolDetail(request: RequestProtocol) async throws -> Result<[SchoolDetailModel], NetworkError> {
        guard let data = try await networkManager.request(request: request) else {
            return .failure(.invalidResponse)
        }
        guard let dataModel = JsonParser.getModelFor(data: data, model: [SchoolDetailModel].self) else {
            return .failure(.parsingError)
        }
        return .success(dataModel)
    }
}
