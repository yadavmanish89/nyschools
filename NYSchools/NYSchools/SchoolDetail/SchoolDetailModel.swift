//
//  SchoolDetailModel.swift
//  NYSchools
//
//  Created by manish yadav on 3/16/23.
//

import Foundation

struct SchoolDetailModel: Codable {
    var dbn: String?
    var schoolName: String?
    var numOfSatTestTakers: String?
    var satCriticalReadingAvgScore: String?
    var satMathAvgScore: String?
    var satWritingAvgScore: String?
}
