//
//  SchoolDetailViewController.swift
//  NYSchools
//
//  Created by manish yadav on 3/16/23.
//

import UIKit

class SchoolDetailViewController: UIViewController {
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var totalSatTakerLabel: UILabel!
    @IBOutlet weak var satMathAvgScoreLabel: UILabel!
    @IBOutlet weak var satReadingAvgScoreLabel: UILabel!
    @IBOutlet weak var satWritinAvgScoreLabel: UILabel!

    var viewModel: SchoolDetailViewModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpViewModel()
        self.loadData()
        self.title = "School"
    }
    
    /// Initialize ViewModel with Network instance
    /// Call back for UI update and to present Error alert
    private func setUpViewModel() {
        
        self.viewModel.updateUI = { [weak self] in
            DispatchQueue.main.async {
                self?.populateData()
            }
        }
        self.viewModel.showError = { [weak self] errorMessage in
            print("Show error message:\(errorMessage)")
            DispatchQueue.main.async {
                self?.presentAlert(title: "Error", message: errorMessage.description)
            }
        }
    }
    private func populateData() {
        self.schoolNameLabel.text = self.viewModel.dataModel?.schoolName
        self.totalSatTakerLabel.text = self.viewModel.dataModel?.numOfSatTestTakers
        self.satMathAvgScoreLabel.text = self.viewModel.dataModel?.satMathAvgScore
        self.satWritinAvgScoreLabel.text = self.viewModel.dataModel?.satWritingAvgScore
        self.satReadingAvgScoreLabel.text = self.viewModel.dataModel?.satCriticalReadingAvgScore
    }
    
    private func loadData() {
        self.viewModel.fetchSchoolDetail(request: APIRequest.schoolDetail(self.viewModel.selectedSchoolDbn))
    }
    
    func presentAlert(title: String,
                      message: String) {
        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: .alert)
        let buttonAction = UIAlertAction(title: "Ok",
                                         style: .cancel) { [weak self] _ in
            self?.popViewController()
        }
        alertController.addAction(buttonAction)
        self.present(alertController, animated: true, completion: nil)
    }

    private func popViewController() {
        self.navigationController?.popViewController(animated: true)
    }
}
