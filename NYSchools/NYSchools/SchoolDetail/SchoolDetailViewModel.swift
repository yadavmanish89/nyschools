//
//  SchoolDetailViewModel.swift
//  NYSchools
//
//  Created by manish yadav on 3/16/23.
//

import Foundation

class SchoolDetailViewModel {
    var selectedSchoolDbn: String
    var dataModel: SchoolDetailModel?
    let schoolDetailNetworkManager: SchoolDetailNetworkManager

    init(dbn: String,
         schoolDetailNetworkManager: SchoolDetailNetworkManager) {
        self.selectedSchoolDbn = dbn
        self.schoolDetailNetworkManager = schoolDetailNetworkManager
    }
    var updateUI: (() -> ())?
    var showError: ((String) -> ())?
    func fetchSchoolDetail(request: RequestProtocol) {
        Task {
            do {
                let result = try await self.schoolDetailNetworkManager.fetchSchoolDetail(request: request)
                switch result {
                case .success(let dataModel):
                    if dataModel.isEmpty == false {
                        self.dataModel = dataModel.first
                        self.updateUI?()
                    } else {
                        self.showError?(NetworkError.emptyValues.description)
                    }
                case .failure(let error):
                    debugPrint("Error:\(error)")
                    self.showError?(error.description)
                }
            } catch(let error) {
                debugPrint("Error:\(error)")
                self.showError?(error.localizedDescription)
            }
        }
    }
}
