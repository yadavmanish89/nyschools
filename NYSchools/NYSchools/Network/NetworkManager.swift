//
//  NetworkProtocol.swift
//  NYSchools
//
//  Created by manish yadav on 3/16/23.
//

import Foundation

class NetworkManager: NetworkProtocol {
    func request(request: RequestProtocol) async throws -> Data?{
        guard let url = request.url else {
            throw NetworkError.invalidURL
        }
        let urlRequest = URLRequest(url: url)
        let (data, response) = try await URLSession.shared.data(for: urlRequest)
        guard let httpResponse = response as? HTTPURLResponse,
              httpResponse.statusCode == 200 else {
            throw NetworkError.invalidResponse
        }
        return data
    }
}
