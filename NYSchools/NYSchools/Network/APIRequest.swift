//
//  NetworkProtocol.swift
//  NYSchools
//
//  Created by manish yadav on 3/16/23.
//

import Foundation

protocol RequestProtocol {
    var url: URL? { get }
    var httpHeaders: [String : String]? { get }
    var method: String { get }
}

let baseURL = "https://data.cityofnewyork.us"
enum APIRequest: RequestProtocol {
    case schoolList
    case schoolDetail(String)
    
    var url: URL? {
        switch self {
        case .schoolList:
            return URL(string: "\(baseURL)/resource/s3k6-pzi2.json")
        case .schoolDetail(let dbn):
            return URL(string: "\(baseURL)/resource/f9bf-2cp4.json?dbn=\(dbn)")
        }
    }
    var httpHeaders: [String: String]? {
        return nil
    }
    var method: String {
        return "GET"
    }
}
