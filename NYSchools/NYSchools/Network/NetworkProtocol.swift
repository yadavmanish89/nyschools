//
//  NetworkProtocol.swift
//  NYSchools
//
//  Created by manish yadav on 3/16/23.
//

import Foundation


protocol NetworkProtocol {
    func request(request: RequestProtocol) async throws -> Data?
}
